import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

import '../ui/shared/theme/chosen_theme.dart';

class AppSharedPreferences {
  static SharedPreferences? _pref;

  AppSharedPreferences();

  _getSharedPreferences() async {
    _pref = await SharedPreferences.getInstance();
  }

  static const String themeKey = "theme";

  Future<void> changeTheme(ChosenTheme chosenTheme) async {
    await _getSharedPreferences();
    _pref?.setInt(themeKey, chosenTheme.index);
  }

  Future<ChosenTheme?> loadTheme() async {
    try {
      await _getSharedPreferences();
      return ChosenTheme.fromOrdinal(_pref?.getInt(themeKey));
    } catch (e) {
      return null;
    }
  }
}