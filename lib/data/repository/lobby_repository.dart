import 'dart:developer';

import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:firebase_database/firebase_database.dart';

class LobbyRepository {
  late final DatabaseReference _lobby;

  // Private constructor and instance getter
  LobbyRepository._(FirebaseDatabase database) {
    _lobby = database.ref("lobby");
  }

  static LobbyRepository? _instance;
  static get instance {
    return _instance ??= LobbyRepository._(FirebaseDatabase.instance);
  }

  // Database path keys
  static const String isWaitingKey = "is_waiting";
  static const String playerBaseKey = "player";
  static const String playerHostKey = "${playerBaseKey}_host";
  static const String playerIdKey = "id";
  static const String playerNameKey = "name";

  // Repository methods
  void getLobbyData(void Function(List<LobbyGameModel> lobbyGames) callback) {
    _lobby.onValue.listen((event) {
      log("getLobbyData triggered");
      callback(_toDomainModel(event.snapshot));
    }).onError(
            (error, stackTrace) => log("getLobbyData failed: ${error.toString()}")
    );
  }

  void addGame(AnonymousUserModel user) async {
    final newGame = _lobby.push();
    final newGameData = {
      isWaitingKey: true,
      playerHostKey: {
        playerIdKey: user.id,
        playerNameKey: user.name
      }
    };
    newGame.set(newGameData)
        .then((value) => log("addGame success"))
        .onError((error, stackTrace) => log("addGame failed: ${error.toString()}"));
  }

  void joinGame(AnonymousUserModel user, String gameId) async {
    final game = await _lobby.child(gameId).get();
    final playerCount = game.children.length - 1;
    if (playerCount < 4) {
      game.child("$playerBaseKey$playerCount").ref.set(
          {playerIdKey: user.id, playerNameKey: user.name}
      )
          .then((value) => log("joinGame success"))
          .onError((error, stackTrace) => log("joinGame failed: ${error.toString()}"));
    }
  }
  
  void cancelGame(String userId, String gameId) async {
    final game = await _lobby.child(gameId).get();
    game.children.where((element) => element.key != isWaitingKey).forEach((player) {
      if (player.child(playerIdKey).value as String == userId) {
        if (player.key == playerHostKey) {
          game.ref.remove();
        } else {
          player.ref.remove();
        }
      }
    });
  }

  void removeGame(String gameId) {
    // There will be ignorable DatabaseException when rejoining because there is no such gameId the lobby
    _lobby.child(gameId).remove();
  }

  void startGame(String gameId) {
    _lobby.child(gameId).child(isWaitingKey).set(false);
  }

  // Firebase-to-domain mappers
  List<LobbyGameModel> _toDomainModel(DataSnapshot snapshot) {
    return snapshot.children.map((game) => LobbyGameModel(
      id: (game.exists)
          ? (game.key ?? LobbyGameModel.noneId)
          : LobbyGameModel.noneId,
      isWaiting: game.child(isWaitingKey).value as bool? ?? true,
      players: game.children
          .where((element) => (
            element.key != isWaitingKey
                && element.child(playerIdKey).value != null
                && element.child(playerNameKey).value != null
          ))
          .map((element) => AnonymousUserModel(
            element.child(playerIdKey).value as String,
            element.child(playerNameKey).value as String
          ))
          .toList(),
      rejoin: false
    )).toList();
  }
}