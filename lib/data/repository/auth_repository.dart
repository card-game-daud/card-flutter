import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final FirebaseAuth _auth;

  AuthRepository._(this._auth);

  static AuthRepository? _instance;
  static AuthRepository get instance {
    _instance ??= AuthRepository._(FirebaseAuth.instance);
    return _instance as AuthRepository;
  }

  AnonymousUserModel? getCurrentUser() {
    return convertToDomainModel(_auth.currentUser);
  }

  Future<AnonymousUserModel?> signInAnonymously() async {
    try {
      await _auth.signInAnonymously();
      // Log.d("AnonymousAuth", "signInAnonymously:success")
      return getCurrentUser();
    } catch (e) {
      // Log.w("AnonymousAuth", "signInAnonymously:failure", e)
      return null;
    }
  }

  Future<AnonymousUserModel?> updateUserName(String newName) async {
    try {
      await _auth.currentUser?.updateDisplayName(newName);
      return getCurrentUser();
    } catch (e) {
      return null;
    }
  }
}

AnonymousUserModel? convertToDomainModel(User? firebaseUser) {
  return (firebaseUser != null)
      ? AnonymousUserModel(
        firebaseUser.uid, firebaseUser.displayName ?? AnonymousUserModel.defaultName
      )
      : null;
}