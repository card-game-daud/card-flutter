import 'dart:developer';

import 'package:firebase_database/firebase_database.dart';

class ConnectionRepository {
  late final DatabaseReference _connectionNode;

  // Private constructor and instance getter
  ConnectionRepository._(FirebaseDatabase database) {
    _connectionNode = database.ref(".info/connected");
  }

  static ConnectionRepository? _instance;
  static get instance {
    return _instance ??= ConnectionRepository._(FirebaseDatabase.instance);
  }

  void listenToConnectionStatus(Function(bool) callback) {
    _connectionNode.onValue.listen((event) {
      final isConnected = (event.snapshot.value as bool?) ?? false;
      switch (isConnected) {
        case true:
          log("updateConnection: connected");
        case false:
          log("updateConnection: disconnected");
      }
      callback(isConnected);
    }).onError((error) => log(error.toString()));
  }
}