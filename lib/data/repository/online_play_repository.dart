import 'dart:async';
import 'dart:developer';

import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:firebase_database/firebase_database.dart';

import '../../domain/model/anonymous_user_model.dart';
import '../../domain/model/card_model.dart';
import '../../domain/model/game_status_model.dart';
import '../../domain/model/player_model.dart';

class OnlinePlayRepository {
  late final DatabaseReference games; // Parent node for all game nodes
  late DatabaseReference game;
  late StreamSubscription<DatabaseEvent> listener;

  // Private constructor and instance getter
  OnlinePlayRepository._(FirebaseDatabase database) {
    games = database.ref("games");
    game = games; // Dummy initial value
  }

  static OnlinePlayRepository? _instance;
  static get instance {
    return _instance ??= OnlinePlayRepository._(FirebaseDatabase.instance);
  }

  // Path keys
  static const String playersKey = "players";
  static const String playerIdKey = "id";
  static const String playerNameKey = "name";
  static const String playerDeckKey = "deck";
  static const String playerDiscardPileKey = "discard_pile";
  static const String playerTurnStatusKey = "turn_status";
  static const String playerScoreKey = "score";
  static const String drawPileKey = "draw_pile";
  static const String curPlayerIdKey = "current_player_id";
  static const String gameStatusKey = "game_status";
  static const String enteredHumansKey = "entered_humans";

  Future<bool> doesGameExist(String gameId) async {
    final game = await games.child(gameId).get();
    return game.value != null;
  }

  Future<LobbyGameModel?> getOngoingGameOfUser(String userKey) async {
    final gameList = await games.get();
    final ongoingGameSnapshot = gameList.children
        .where((game) {
          final playerKeys = game.child(playersKey).children.map((player) => player.key);
          return playerKeys.contains(userKey);
        })
        .firstOrNull;
    return (ongoingGameSnapshot != null) ? _mapOngoingGameData(ongoingGameSnapshot) : null;
  }

  LobbyGameModel _mapOngoingGameData(DataSnapshot game) {
    return LobbyGameModel(
        id: (game.exists) ? (game.key ?? LobbyGameModel.noneId) : LobbyGameModel.noneId,
        isWaiting: false,
        players: game.child(playersKey).children
            .where((player) {
              final isComputerPlayer =
                  player.key?.startsWith(PlayerModel.computerPlayerPrefix)
                      ?? false;
              return !isComputerPlayer;
            })
            .map((player) => _mapOngoingUserToModel(player))
            .toList(),
        rejoin: true
    );
  }

  AnonymousUserModel _mapOngoingUserToModel(DataSnapshot user) {
    return AnonymousUserModel(
        user.key!,
        user.child(playerNameKey).value as String
    );
  }

  String _playerPath(String playerId, String key) {
    return "$playersKey/$playerId/$key";
  }

  void syncGameData(
      String gameId,
      void Function(
          List<PlayerModel> playerData, 
          List<CardModel> drawPile, 
          int curPlayerId, 
          GameStatus gameStatus
      ) callback
  ) {
    if (gameId.isNotEmpty) {
      listener = games.child(gameId).onValue.listen((event) {
        log("syncGameData called");
        if (_validateGameData(event.snapshot)) {
          final (playerData, drawPile, curPlayerId, gameStatus) = _convertGameData(event.snapshot);
          callback(playerData, drawPile, curPlayerId, gameStatus);
          log("syncGameData success");
        }
      });
      listener.onError((error) => log(error.toString()));
    }
  }

  bool _validateGameData(DataSnapshot snapshot) {
    final validParent = snapshot.ref.parent?.key == games.key; // Has to be a game node
    final hasPlayersChild = snapshot.child(playersKey).exists;
    final hasCurPlayerIdChild = snapshot.child(curPlayerIdKey).exists;
    final hasGameStatusChild = snapshot.child(gameStatusKey).exists;
    // Draw pile can be empty and doesn't have to be checked

    log("[validateGameData] validParent: $validParent,"
        " hasPlayersChild: $hasPlayersChild"
        " hasCurPlayerIdChild: $hasCurPlayerIdChild"
        " hasGameStatusChild: $hasGameStatusChild");
    return validParent && hasPlayersChild && hasCurPlayerIdChild && hasGameStatusChild;
  }

  (
    List<PlayerModel> playerData,
    List<CardModel> drawPile,
    int curPlayerId,
    GameStatus gameStatus
  ) _convertGameData(DataSnapshot snapshot) {
    final playerData = snapshot.child(playersKey).children.map(
            (player) => PlayerModel(
            player.child(playerIdKey).value as int,
            player.child(playerNameKey).value as String,
            (player.key!.startsWith("COMP ")) ? null : player.key as String,
            _getCardList(player.child(playerDeckKey)),
            _getCardList(player.child(playerDiscardPileKey)),
            TurnStatus.values[player.child(playerTurnStatusKey).value as int],
            player.child(playerScoreKey).value as int? ?? 0
        )
    ).toList();
    playerData.sort((player1, player2) => player1.id.compareTo(player2.id));
    return (
      playerData,
      _getCardList(snapshot.child(drawPileKey)),
      snapshot.child(curPlayerIdKey).value as int,
      GameStatus.values[snapshot.child(gameStatusKey).value as int]
    );
  }

  List<CardModel> _getCardList(DataSnapshot snapshot) {
    if (snapshot.exists) {
      final cardIndices = snapshot.value as List<Object?>;
      return cardIndices.nonNulls.map(
              (index) => CardModel.values[index as int]
      ).toList();
    } else {
      return List.empty(growable: true);
    }
  }

  List<int> _cardListToOrdinals(List<CardModel> cardList) {
    return cardList.map((card) => card.index).toList();
  }

  Future<void> enterGame(String gameId, String userKey, void Function(int) callback) async {
    if (gameId.isNotEmpty) {
      game = games.child(gameId);
      await game.child(enteredHumansKey).child(userKey).set("entered")
          .then((value) => game.child(enteredHumansKey).get()
            .then((value) => callback(value.children.length))
          );
    }
  }

  Future<(List<PlayerModel>, List<CardModel>, int, GameStatus)> startGame(
      String gameId,
      List<PlayerModel> startingPlayerData,
      List<CardModel> startingDrawPile,
      int startingPlayer,
  ) async {
    final startGameData = _createStartGameData(startingPlayerData, startingDrawPile, startingPlayer);
    final gameRef = games.child(gameId);
    game = gameRef;
    await gameRef.update(startGameData)
        .then((value) => log("startGame success"))
        .catchError((error) => log("startGame error: ${error.toString()}"));
    return _convertGameData(await gameRef.get());
  }

  Map<String, Object?> _createStartGameData(
    List<PlayerModel> startingPlayerData,
    List<CardModel> startingDrawPile,
    int startingPlayer,
  ) {
    final startGameData = <String, Object?>{};
    startGameData[drawPileKey] = _cardListToOrdinals(startingDrawPile);
    startGameData[gameStatusKey] = GameStatus.ongoing.index;
    startGameData[curPlayerIdKey] = startingPlayer;
    final playersData = <String, Object?>{};
    for (var id = 0; id < startingPlayerData.length; id++) {
      final playerData = <String, Object?>{};
      playerData[playerIdKey] = id;
      playerData[playerNameKey] = startingPlayerData[id].name;
      playerData[playerDeckKey] = _cardListToOrdinals(startingPlayerData[id].deck);
      playerData[playerDiscardPileKey] = null;
      playerData[playerTurnStatusKey] = (id == startGameData["current_player_id"])
          ? TurnStatus.drawing.index : TurnStatus.waiting.index;
      playerData[playerScoreKey] = null;
      playersData[startingPlayerData[id].uid ?? "COMP $id"] = playerData;
    }
    startGameData[playersKey] = playersData;
    return startGameData;
  }

  Future<void> drawFromDrawPile(
    List<PlayerModel> updatedPlayersData,
    List<CardModel> updatedDrawPile,
  ) async {
    if (game != games) {
      final updateData = <String, Object?>{};
      for (final player in updatedPlayersData) {
        final id = player.uid ?? "COMP ${player.id}";
        updateData[_playerPath(id, playerDeckKey)] = _cardListToOrdinals(player.deck);
        updateData[_playerPath(id, playerTurnStatusKey)] = player.turnStatus.index;
      }
      updateData[drawPileKey] = _cardListToOrdinals(updatedDrawPile);
      await game.update(updateData)
          .then((value) => log("drawFromDrawPile success"))
          .catchError((error) => log("drawFromDrawPile error: ${error.toString()}"));
    }
  }

  Future<void> drawFromDiscardPile(
      List<PlayerModel> updatedPlayersData,
  ) async {
    if (game != games) {
      final updateData = <String, Object?>{};
      for (final player in updatedPlayersData) {
        final id = player.uid ?? "COMP ${player.id}";
        updateData[_playerPath(id, playerDeckKey)] = _cardListToOrdinals(player.deck);
        updateData[_playerPath(id, playerDiscardPileKey)] = _cardListToOrdinals(player.discardPile);
        updateData[_playerPath(id, playerTurnStatusKey)] = player.turnStatus.index;
      }
      await game.update(updateData)
          .then((value) => log("drawFromDiscardPile success"))
          .catchError((error) => log("drawFromDiscardPile error: ${error.toString()}"));
    }
  }

  Future<void> discardCard(
      List<PlayerModel> updatedPlayersData,
      int nextPlayerId
  ) async {
    if (game != games) {
      final updateData = <String, Object?>{};
      for (final player in updatedPlayersData) {
        final id = player.uid ?? "COMP ${player.id}";
        updateData[_playerPath(id, playerDeckKey)] = _cardListToOrdinals(player.deck);
        updateData[_playerPath(id, playerDiscardPileKey)] = _cardListToOrdinals(player.discardPile);
        updateData[_playerPath(id, playerTurnStatusKey)] = player.turnStatus.index;
      }
      updateData[curPlayerIdKey] = nextPlayerId;
      await game.update(updateData)
          .then((value) => log("discardCard success"))
          .catchError((error) => log("discardCard error: ${error.toString()}"));
    }
  }

  Future<void> updateGameStatus(GameStatus gameStatus) async {
    if (game != games) {
      game.child(gameStatusKey).set(gameStatus.index);
    }
  }

  Future<void> endGame(List<PlayerModel> finalPlayersData) async {
    if (game != games) {
      final updateData = <String, Object?>{};
      updateData[gameStatusKey] = GameStatus.finishing.index;
      for (final player in finalPlayersData) {
        final id = player.uid ?? "COMP ${player.id}";
        updateData[_playerPath(id, playerScoreKey)] = player.score;
      }
      await game.update(updateData)
          .then((value) => log("endGame success"))
          .catchError((error) => log("endGame error: ${error.toString()}"));
    }
  }

  Future<void> leaveGame(String userKey) async {
    if (game != games) {
      await listener.cancel();
      final enteredHumans = await game.child(enteredHumansKey).get();
      if (enteredHumans.children.length <= 1) {
        await game.remove()
            .then((value) => log("leaveGame success"))
            .catchError((error) => log("leaveGame error: ${error.toString()}"));
      } else {
        await enteredHumans.ref.child(userKey).remove()
            .then((value) => log("leaveGame success"))
            .catchError((error) => log("leaveGame error: ${error.toString()}"));
      }
    }
  }
}