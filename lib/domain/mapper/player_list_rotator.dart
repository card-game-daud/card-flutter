// Rotators for a specific position
import '../model/player_model.dart';

int rotateFromRemote(int index, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  return (index - userRelativeId + 4) % 4;
}

int rotateToRemote(int index, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  return (index + userRelativeId) % 4;
}

// Rotators for a list
List<PlayerModel> rotateListFromRemote(List<PlayerModel> list, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  final rotation = userRelativeId;
  return list.sublist(rotation)..addAll(list.sublist(0, rotation));
}

List<PlayerModel> rotateListToRemote(List<PlayerModel> list, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  final rotation = list.length - userRelativeId;
  return list.sublist(rotation)..addAll(list.sublist(0, rotation));
}

List<(String?, String)> rotatePairListFromRemote(List<(String?, String)> list, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  final rotation = userRelativeId;
  return list.sublist(rotation)..addAll(list.sublist(0, rotation));
}

List<(String?, String)> rotatePairListToRemote(List<(String?, String)> list, int userRelativeId) {
  assert(userRelativeId >= 0 && userRelativeId <= 4);
  final rotation = list.length - userRelativeId;
  return list.sublist(rotation)..addAll(list.sublist(0, rotation));
}