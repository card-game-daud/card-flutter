import '../../data/repository/auth_repository.dart';
import '../model/anonymous_user_model.dart';

class ChangeUserNameUseCase {
  final AuthRepository authRepository;

  ChangeUserNameUseCase(this.authRepository);

  Future<AnonymousUserModel?> invoke(String newName) async {
    return await authRepository.updateUserName(newName);
  }
}