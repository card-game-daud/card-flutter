import 'package:card_flutter/data/repository/auth_repository.dart';
import 'package:card_flutter/domain/model/anonymous_user_model.dart';

class GetAnonymousUserUseCase {
  final AuthRepository authRepository;

  GetAnonymousUserUseCase(this.authRepository);

  Future<AnonymousUserModel?> invoke() async {
    var currentUser = authRepository.getCurrentUser();
    return currentUser ?? await authRepository.signInAnonymously();
  }
}