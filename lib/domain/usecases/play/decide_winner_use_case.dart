import '../../model/player_model.dart';

class DecideWinnerUseCase {
  //TODO: Handle draws (do the same for Compose version at the same time

  PlayerModel? invoke(List<PlayerModel> playerList) {
    var maxScore = -31; // Initial value is the lowest possible score minus 1
    PlayerModel? winner;
    // Calculate the scores for each suit as the primary suit
    for (final player in playerList) {
      if (player.score > maxScore) {
        maxScore = player.score;
        winner = player;
      }
    }
    return winner;
  }
}