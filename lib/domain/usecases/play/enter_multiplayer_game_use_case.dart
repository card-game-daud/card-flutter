import 'package:card_flutter/data/repository/lobby_repository.dart';
import 'package:card_flutter/data/repository/online_play_repository.dart';

class EnterMultiplayerGameUseCase {
  final LobbyRepository _lobbyRepository;
  final OnlinePlayRepository _onlinePlayRepository;

  EnterMultiplayerGameUseCase(this._lobbyRepository, this._onlinePlayRepository);

  void invoke(String gameId, String userKey, int supposedHumanCount) {
    _onlinePlayRepository.enterGame(gameId, userKey, (humanCount) {
      if (humanCount == supposedHumanCount) {
        _lobbyRepository.removeGame(gameId);
      }
    });
  }
}