import 'dart:math';

import '../../model/card_model.dart';
import '../../model/player_model.dart';

class GenerateComputerMoveUseCase {
  (DrawSource, CardModel) invoke(PlayerModel playerData) {
    return _randomMove(playerData);
  }

  (DrawSource, CardModel) _randomMove(PlayerModel playerData) {
    final drawSource = (playerData.discardPile.isEmpty)
        ? DrawSource.drawPile
        : DrawSource.values[Random().nextInt(DrawSource.values.length)];
    final discardedCard = playerData.deck[Random().nextInt(playerData.deck.length)];
    return (drawSource, discardedCard);
  }
}

enum DrawSource {
  drawPile, discardPile
}