class AnonymousUserModel {
  final String id;
  String name;

  AnonymousUserModel(this.id, this.name);

  static const defaultName = "PLAYER";
}