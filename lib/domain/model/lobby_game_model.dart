import 'anonymous_user_model.dart';

class LobbyGameModel {
  final String id;
  bool isWaiting;
  List<AnonymousUserModel> players;
  bool rejoin;

  LobbyGameModel({
    required this.id,
    required this.isWaiting,
    required this.players,
    required this.rejoin
  });

  // Static variables
  static const noneId = "none";
  static const multiplayerPrefix = "-";

  // Convenience getters
  AnonymousUserModel get host => players.last;
  Iterable<AnonymousUserModel> get nonHostPlayers =>
      players.getRange(0, players.length - 1);
  Iterable<String> get playerIds => players.map((player) => player.id);
  bool get isMultiplayer => id.startsWith(LobbyGameModel.multiplayerPrefix);
}