import 'package:card_flutter/data/repository/auth_repository.dart';
import 'package:card_flutter/data/repository/connection_repository.dart';
import 'package:card_flutter/data/repository/lobby_repository.dart';
import 'package:card_flutter/data/repository/online_play_repository.dart';
import 'package:card_flutter/data/shared_preferences.dart';
import 'package:card_flutter/domain/usecases/change_user_name_use_case.dart';
import 'package:card_flutter/domain/usecases/get_anonymous_user_use_case.dart';
import 'package:card_flutter/domain/usecases/play/decide_winner_use_case.dart';
import 'package:card_flutter/domain/usecases/play/enter_multiplayer_game_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_computer_move_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_game_data_use_case.dart';
import 'package:card_flutter/ui/home/home_screen.dart';
import 'package:card_flutter/ui/home/home_view_model.dart';
import 'package:card_flutter/ui/lobby/lobby_view_model.dart';
import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:card_flutter/ui/shared/theme/theme.dart';
import 'package:card_flutter/ui/shared/view_models/connection_view_model.dart';
import 'package:card_flutter/ui/shared/view_models/theme_view_model.dart';
import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await FirebaseAppCheck.instance.activate(
    androidProvider: AndroidProvider.playIntegrity,
  );
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ThemeViewModel(AppSharedPreferences())),
        ChangeNotifierProvider(create: (context) => UserViewModel(
            GetAnonymousUserUseCase(AuthRepository.instance),
            ChangeUserNameUseCase(AuthRepository.instance)
        )),
        ChangeNotifierProvider(create: (context) => ConnectionViewModel(
          ConnectionRepository.instance
        )),
        ChangeNotifierProvider(create: (context) => HomeViewModel(OnlinePlayRepository.instance)),
        ChangeNotifierProvider(create: (context) => PlayViewModel(
          GetAnonymousUserUseCase(AuthRepository.instance),
          GenerateGameDataUseCase(),
          GenerateComputerMoveUseCase(),
          DecideWinnerUseCase(),
          EnterMultiplayerGameUseCase(
              LobbyRepository.instance,
              OnlinePlayRepository.instance
          ),
          OnlinePlayRepository.instance
        )),
        ChangeNotifierProvider(create: (context) => LobbyViewModel(
          LobbyRepository.instance
        )),
      ],
      child: const MyApp()
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: lightColorScheme,
          useMaterial3: true,
        ),
        darkTheme: ThemeData(
          colorScheme: darkColorScheme,
          useMaterial3: true,
        ),
        themeMode: Provider.of<ThemeViewModel>(context).currentTheme.toThemeMode(),
        home: const MyHomePage(),
      );
  }
}
