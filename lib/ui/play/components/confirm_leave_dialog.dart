import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConfirmLeaveDialog extends StatefulWidget {
  final bool askToSave;
  final void Function(bool) onConfirm;

  const ConfirmLeaveDialog({
    super.key,
    required this.askToSave,
    required this.onConfirm
  });

  @override
  State<StatefulWidget> createState() => _ConfirmLeaveDialogState();
}

class _ConfirmLeaveDialogState extends State<ConfirmLeaveDialog> {
  bool chooseSave = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Leave Game?",),
      content: Consumer<PlayViewModel>(
        builder: (BuildContext context, PlayViewModel vm, Widget? child) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                "Apakah Anda yakin Anda ingin meninggalkan permainan?",
                textAlign: TextAlign.justify,
                softWrap: true,
              ),
              SizedBox(height: widget.askToSave ? 16.0 : 8.0,),
              if (!widget.askToSave) const Text(
                "Jika Anda pergi, permainan ini akan dihapus selamanya.",
                textAlign: TextAlign.justify,
                softWrap: true,
              ),
              if (widget.askToSave) Row(
                children: [
                  SizedBox(
                    height: 4.0,
                    child: Checkbox(
                      value: chooseSave,
                      onChanged: (newChoice) {
                        if (newChoice != null) {
                          setState(() {
                            chooseSave = newChoice;
                          });
                        }
                      },
                    ),
                  ),
                  InkWell(
                    child: const Text("Simpan permainan saat keluar"),
                    onTap: () => setState(() {
                      chooseSave = !chooseSave;
                    }),
                  )
                ],
              )
            ],
          );
        },
      ),
      actions: [
        OutlinedButton(
            onPressed: () {
              Navigator.pop(context); // Pop ConfirmLeaveDialog
              widget.onConfirm(chooseSave); // Execute code depending on parent
            },
            child: const Text("Ya")
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("Tidak")
        )
      ],
    );
  }
}