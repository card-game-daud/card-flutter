import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/ui/play/components/card_components.dart';
import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'confirm_leave_dialog.dart';

class GameOverDialog extends StatelessWidget {
  final PlayMode mode;
  final void Function(bool) onExit;

  const GameOverDialog({super.key, required this.mode, required this.onExit});

  @override
  Widget build(BuildContext context) {
    return PopScope(
        canPop: false,
        child: AlertDialog(
          title: const Text("Game Over", textAlign: TextAlign.center,),
          content: SingleChildScrollView(
            child: UnconstrainedBox(
              child: Consumer<PlayViewModel>(
                builder: (BuildContext context, PlayViewModel vm, Widget? child) {
                  return Wrap(
                    direction: Axis.vertical,
                    spacing: 16.0,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      ...List.generate(PlayViewModel.numPlayers, (index) =>
                          _PlayerScore(playerData: vm.playerData![index])
                      ),
                      _WinnerText(name: vm.winner!.name)
                    ],
                  );
                },
              ),
            )
          ),
          actions: [
            if (mode != PlayMode.multiplayer) ElevatedButton(
                onPressed: () => _onClickRestart(context),
                child: const Text("Restart")
            ),
            ElevatedButton(
                onPressed: () => _onClickExit(context),
                child: const Text("Exit")
            )
          ],
          actionsAlignment: MainAxisAlignment.center,
        )
    );
  }

  void _onClickRestart(BuildContext context) {
    Provider.of<PlayViewModel>(context, listen: false).restartGame(mode);
    Navigator.pop(context);
  }

  void _onClickExit(BuildContext context) {
    if (mode != PlayMode.multiplayer) {
      showDialog(context: context, builder: (context) =>
          ConfirmLeaveDialog(
            askToSave: mode == PlayMode.online,
            onConfirm: (chooseSave) {
              Navigator.pop(context); // Pop this dialog
              onExit(chooseSave);
            },
          )
      );
    } else {
      Navigator.pop(context); // Pop this dialog
      onExit(false);
    }
  }
}

class _PlayerScore extends StatelessWidget {
  final PlayerModel playerData;

  const _PlayerScore({required this.playerData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text.rich(
          TextSpan(
              children: [
                TextSpan(
                    text: "${playerData.name}:",
                    style: const TextStyle(fontWeight: FontWeight.bold)
                ),
                TextSpan(text: " ${playerData.score}")
              ],
              style: Theme.of(context).textTheme.bodyLarge
          ),
        ),
        PlayingCardDeck(listCard: playerData.deck, isHidden: false, isVertical: false)
      ],
    );
  }
}

class _WinnerText extends StatelessWidget {
  final String name;
  
  const _WinnerText({required this.name});
  
  @override
  Widget build(BuildContext context) {
    return Text.rich(
        TextSpan(
            children: [
              const TextSpan(
                  text: "Winner:",
                  style: TextStyle(fontWeight: FontWeight.bold)
              ),
              TextSpan(text: " $name")
            ],
            style: Theme.of(context).textTheme.bodyLarge
        )
    );
  }
}