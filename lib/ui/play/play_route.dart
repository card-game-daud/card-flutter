import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/ui/play/play_screen.dart';
import 'package:flutter/material.dart';

class PlayArguments {
  LobbyGameModel? gameData;

  PlayArguments({this.gameData});
}

class PlayScreenRoute {
  static Route generateOfflinePageRoute() =>
      MaterialPageRoute(builder: (context) => PlayScreen.offline());

  static Route generateOnlinePageRoute() =>
      MaterialPageRoute(builder: (context) => PlayScreen.online());

  static Route generateMultiPlayerPageRoute({
    required LobbyGameModel gameData
  }) {
    return MaterialPageRoute(
        builder: (context) => PlayScreen.multiplayer(),
        settings: RouteSettings(arguments: PlayArguments(
            gameData: gameData
        ))
    );
  }
}