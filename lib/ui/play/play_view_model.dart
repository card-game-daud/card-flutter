import 'dart:async';

import 'package:card_flutter/data/repository/online_play_repository.dart';
import 'package:card_flutter/domain/mapper/player_list_rotator.dart';
import 'package:card_flutter/domain/model/card_model.dart';
import 'package:card_flutter/domain/model/game_status_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/domain/usecases/get_anonymous_user_use_case.dart';
import 'package:card_flutter/domain/usecases/play/decide_winner_use_case.dart';
import 'package:card_flutter/domain/usecases/play/enter_multiplayer_game_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_computer_move_use_case.dart';
import 'package:card_flutter/domain/usecases/play/generate_game_data_use_case.dart';
import 'package:flutter/material.dart';

class PlayViewModel extends ChangeNotifier {
  // Dependencies
  final GetAnonymousUserUseCase _getAnonymousUser;
  final GenerateGameDataUseCase _generateGameData;
  final GenerateComputerMoveUseCase _generateComputerMove;
  final DecideWinnerUseCase _decideWinner;
  final EnterMultiplayerGameUseCase _enterMultiplayerGame;
  final OnlinePlayRepository _onlinePlayRepository;

  // States
  GameStatus? _gameStatus = GameStatus.starting;
  List<CardModel>? _drawPile;
  List<PlayerModel>? _playerData;
  int? _currentPlayerId;
  int? _userRelativeId;
  int? _hostId;

  // Timer to avoid multiple computer moves being executed at once
  Timer? _computerMoveTimer;

  // Constants
  static const numPlayers = 4;
  static const userId = 0;
  static const computerMoveDuration = Duration(milliseconds: 500);

  PlayViewModel(
      this._getAnonymousUser,
      this._generateGameData,
      this._generateComputerMove,
      this._decideWinner,
      this._enterMultiplayerGame,
      this._onlinePlayRepository,
  );

  // Getter for states that need to be accessed by the views
  GameStatus? get gameStatus => _gameStatus;
  List<CardModel>? get drawPile => _drawPile;
  List<PlayerModel>? get playerData => _playerData;

  bool _isHost(PlayMode mode) {
    if (mode == PlayMode.multiplayer) {
      return userId == _hostId;
    } else {
      return true;
    }
  }

  bool isCurrentPlayer(int id) {
    return id == _currentPlayerId;
  }

  void _updateGameData(
      GameStatus gameStatus,
      List<CardModel> drawPile,
      List<PlayerModel> playerData,
      int currentPlayerId
  ) {
    _gameStatus = gameStatus;
    _drawPile = drawPile;
    _playerData = playerData;
    _currentPlayerId = currentPlayerId;
    notifyListeners();
  }

  void setupGame({required PlayMode mode, LobbyGameModel? startingData, String? userKey}) {
    if (startingData != null && startingData.players.isNotEmpty) {
      _setupMultiplayer(startingData, userKey!);
      if (startingData.host.id == userKey && !startingData.rejoin) {
        startGame(mode: mode, lobbyGameData: startingData, userRelativeId: _userRelativeId!);
      } else {
        _listenToOnlineGameData(gameId: startingData.id, userRelativeId: _userRelativeId!);
      }
    } else {
      // If null, then this is single-player
      startGame(mode: mode);
    }
  }

  void _setupMultiplayer(LobbyGameModel startingData, String userKey) {
    final listPlayerIds = startingData.playerIds.toList();
    final userRelativeId = listPlayerIds.indexOf(userKey);

    _hostId = rotateFromRemote(listPlayerIds.length - 1, userRelativeId);
    _userRelativeId = userRelativeId;

    _enterMultiplayerGame.invoke(startingData.id, userKey, listPlayerIds.length);

    // isSetupDone = true
  }

  void _listenToOnlineGameData({String gameId = "", int userRelativeId = 0}) {
    _onlinePlayRepository.syncGameData(gameId, (playerData, drawPile, curPlayerId, gameStatus) {
      _updateGameData(
          gameStatus,
          drawPile,
          rotateListFromRemote(playerData, userRelativeId),
          rotateFromRemote(curPlayerId, userRelativeId)
      );
    });
  }

  Future<void> startGame({LobbyGameModel? lobbyGameData, required PlayMode mode, int userRelativeId = 0}) async {
    final userData = await _getAnonymousUser.invoke();
    final (playerData, drawPile, currentPlayerId) = _generateGameData.invoke(
        numPlayers, lobbyGameData, userData
    );

    switch (mode) {
      case PlayMode.offline:
        _updateGameData(GameStatus.ongoing, drawPile, playerData, currentPlayerId);

      case PlayMode.online:
        final gameId = userData?.id ?? PlayerModel.defaultHumanId;
        final hasSavedGame = await _onlinePlayRepository.doesGameExist(gameId);
        await _onlinePlayRepository.enterGame(gameId, gameId, (playerCount) { /* Do nothing */ });
        // The second expression is for restarting the game, since the game already exists
        if (!hasSavedGame || gameStatus == GameStatus.starting) {
          final gameData = await _onlinePlayRepository.startGame(gameId, playerData, drawPile, currentPlayerId);
          _updateGameData(gameData.$4, gameData.$2, gameData.$1, gameData.$3);
        }

        _listenToOnlineGameData(gameId: gameId);

      case PlayMode.multiplayer:
        final gameData = await _onlinePlayRepository.startGame(lobbyGameData!.id, playerData, drawPile, currentPlayerId);
        _updateGameData(
            gameData.$4,
            gameData.$2,
            rotateListFromRemote(gameData.$1, userRelativeId),
            rotateFromRemote(gameData.$3, userRelativeId)
        );

        _listenToOnlineGameData(gameId: lobbyGameData.id, userRelativeId: userRelativeId);
    }

    if (currentPlayerId != userId && !playerData[currentPlayerId].isHuman() && _isHost(mode)) {
      Timer(computerMoveDuration, () => _computerMove(currentPlayerId, mode));
    }
  }

  Future<void> drawFromDrawPile(int curPlayerId, CardModel drawnCard, PlayMode mode) async {
    final updatedPlayerData = playerData!;
    updatedPlayerData[curPlayerId].deck.add(drawnCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.discarding;
    final updatedDrawPile = drawPile!;
    updatedDrawPile.remove(drawnCard);

    switch (mode) {
      case PlayMode.offline:
        _updateGameData(gameStatus!, updatedDrawPile, updatedPlayerData, _currentPlayerId!);

      case PlayMode.online:
        await _onlinePlayRepository.drawFromDrawPile(updatedPlayerData, updatedDrawPile);

      case PlayMode.multiplayer:
        await _onlinePlayRepository.drawFromDrawPile(
            rotateListToRemote(updatedPlayerData, _userRelativeId!), updatedDrawPile
        );
    }
  }

  Future<void> drawFromDiscardPile(int curPlayerId, CardModel drawnCard, PlayMode mode) async {
    final updatedPlayerData = playerData!;
    updatedPlayerData[curPlayerId].deck.add(drawnCard);
    updatedPlayerData[curPlayerId].discardPile.remove(drawnCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.discarding;

    switch (mode) {
      case PlayMode.offline:
        _updateGameData(gameStatus!, drawPile!, updatedPlayerData, curPlayerId);

      case PlayMode.online:
        await _onlinePlayRepository.drawFromDiscardPile(updatedPlayerData);

      case PlayMode.multiplayer:
        await _onlinePlayRepository.drawFromDiscardPile(
            rotateListToRemote(updatedPlayerData, _userRelativeId!)
        );
    }
  }

  Future<void> discardCard(int curPlayerId, CardModel discardedCard, PlayMode mode) async {
    final updatedPlayerData = playerData!;
    final nextPlayerId = (curPlayerId + 1) % numPlayers;
    updatedPlayerData[curPlayerId].deck.remove(discardedCard);
    updatedPlayerData[curPlayerId].turnStatus = TurnStatus.waiting;
    updatedPlayerData[nextPlayerId].discardPile.add(discardedCard);
    updatedPlayerData[nextPlayerId].turnStatus = TurnStatus.drawing;

    switch (mode) {
      case PlayMode.offline:
        _updateGameData(gameStatus!, drawPile!, updatedPlayerData, nextPlayerId);

      case PlayMode.online:
        await _onlinePlayRepository.discardCard(updatedPlayerData, nextPlayerId);

      case PlayMode.multiplayer:
        await _onlinePlayRepository.discardCard(
            rotateListToRemote(updatedPlayerData, _userRelativeId!),
            rotateToRemote(nextPlayerId, _userRelativeId!)
        );
    }

    if (
      updatedPlayerData[curPlayerId].calculateScore() == PlayerModel.maxScore || drawPile!.isEmpty
    ) {
      _endGame(mode);
    } else {
      if (!updatedPlayerData[nextPlayerId].isHuman() && _isHost(mode)) {
        Timer(computerMoveDuration, () => _computerMove(nextPlayerId, mode));
      }
    }
  }

  void _computerMove(int playerId, PlayMode mode) {
    final computerMove = _generateComputerMove.invoke(playerData![playerId]);

    if (_computerMoveTimer != null) {
      _computerMoveTimer?.cancel();
    }

    _computerMoveTimer = Timer(computerMoveDuration, () async {
      switch (computerMove.$1) {
        case DrawSource.drawPile:
          await drawFromDrawPile(playerId, drawPile!.last, mode);
        case DrawSource.discardPile:
          await drawFromDiscardPile(playerId, playerData![playerId].discardPile.last, mode);
      }

      Timer(computerMoveDuration, () => discardCard(playerId, computerMove.$2, mode));
    });
  }

  Future<void> _endGame(PlayMode mode) async {
    final updatedPlayerData = playerData!;
    for (var i = 0; i < updatedPlayerData.length; i++) {
      updatedPlayerData[i].setScore();
    }

    switch (mode) {
      case PlayMode.offline:
        _updateGameData(GameStatus.finishing, drawPile!, updatedPlayerData, _currentPlayerId!);

      case PlayMode.online:
        await _onlinePlayRepository.endGame(updatedPlayerData);

      case PlayMode.multiplayer:
        await _onlinePlayRepository.endGame(
            rotateListToRemote(updatedPlayerData, _userRelativeId!)
        );
    }
  }

  void restartGame(PlayMode mode) {
    switch (mode) {
      case PlayMode.offline:
        _updateGameData(GameStatus.starting, drawPile!, playerData!, _currentPlayerId!);

      case PlayMode.online:
        _onlinePlayRepository.updateGameStatus(GameStatus.starting);

      case PlayMode.multiplayer:
        throw Exception("Multiplayer games should not be restartable");
    }
  }

  void doneShowingGameOverDialog() {
    _gameStatus = GameStatus.gameover;
  }

  PlayerModel? get winner {
    return _decideWinner.invoke(playerData!);
  }

  Future<void> leaveScreen(PlayMode mode, bool chooseSave) async {
    // If an online game is not saved, remove it from RTDB
    if (!chooseSave && mode != PlayMode.offline) {
      await _onlinePlayRepository.leaveGame(playerData![userId].uid!);
    }

    // Manually reset states
    // Use delay to make sure the screen has been left first
    final int delay = (mode == PlayMode.multiplayer) ? 3 : 1;
    Timer(Duration(seconds: delay), () {
      _gameStatus = GameStatus.starting;
      _drawPile = null;
      _playerData = null;
      _currentPlayerId = null;
      _computerMoveTimer = null;
      notifyListeners();
    });
  }
}

enum PlayMode {
  offline, online, multiplayer
}