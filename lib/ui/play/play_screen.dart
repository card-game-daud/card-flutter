import 'package:card_flutter/domain/model/card_model.dart';
import 'package:card_flutter/domain/model/game_status_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/ui/play/components/card_components.dart';
import 'package:card_flutter/ui/play/components/game_over_dialog.dart';
import 'package:card_flutter/ui/play/components/player_area.dart';
import 'package:card_flutter/ui/play/play_route.dart';
import 'package:card_flutter/ui/play/play_view_model.dart';
import 'package:card_flutter/ui/shared/components/disconnected_dialog.dart';
import 'package:card_flutter/ui/shared/theme/theme.dart';
import 'package:card_flutter/ui/shared/view_models/connection_view_model.dart';
import 'package:card_flutter/ui/shared/view_models/theme_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import '../shared/view_models/user_view_model.dart';
import 'components/confirm_leave_dialog.dart';

class PlayScreen extends StatefulWidget {
  late final PlayMode mode;

  PlayScreen.offline({super.key}) { mode = PlayMode.offline; }
  PlayScreen.online({super.key}) { mode = PlayMode.online; }
  PlayScreen.multiplayer({super.key}) { mode = PlayMode.multiplayer; }

  @override
  State<StatefulWidget> createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  bool showDisconnectedDialog = false;

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)?.settings.arguments as PlayArguments?;
    final startingData = arguments?.gameData;

    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) {
        if (widget.mode != PlayMode.multiplayer && !didPop) {
          _showConfirmLeaveDialog();
        }
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: _getPlayScreenBackground(context)
        ),
        child: Consumer2<PlayViewModel, ConnectionViewModel>(
          builder: (context, playViewModel, connectionViewModel, w) {
            // For smart cast purpose
            final playerData = playViewModel.playerData;
            final drawPileTop = playViewModel.drawPile?.lastOrNull;

            SchedulerBinding.instance.addPostFrameCallback((timeStamp)
              => _launchGameStatusSideEffect(playViewModel.gameStatus, startingData)
            );
            SchedulerBinding.instance.addPostFrameCallback(
              (timeStamp) => _listenForConnectionStatus(connectionViewModel)
            );

            return Stack(
              children: (playerData != null) ? [
                Container(
                  alignment: Alignment.bottomCenter,
                  child: PlayerArea(
                    id: 0,
                    isUser: true,
                    mode: widget.mode,
                    isVertical: false,
                    flipLayout: false,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: PlayerArea(
                    id: 1,
                    isUser: false,
                    mode: widget.mode,
                    isVertical: true,
                    flipLayout: true,
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: PlayerArea(
                    id: 2,
                    isUser: false,
                    mode: widget.mode,
                    isVertical: false,
                    flipLayout: true,
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: PlayerArea(
                    id: 3,
                    isUser: false,
                    mode: widget.mode,
                    isVertical: true,
                    flipLayout: false,
                  ),
                ),
                if (drawPileTop != null) Container(
                  alignment: Alignment.center,
                  child: PlayingCard(
                    card: drawPileTop,
                    isHidden: true,
                    onTap: () => _drawFromDrawPile(playerData[0], drawPileTop),
                  ),
                ),
              ] : List.empty(),
            );
          },
        )
      )
    );
  }
  
  RadialGradient _getPlayScreenBackground(BuildContext context) {
    return (Provider.of<ThemeViewModel>(context).currentTheme.isDark(context))
        ? playScreenBackgroundDark : playScreenBackgroundLight;
  }

  void _showConfirmLeaveDialog() {
    showDialog(context: context, builder: (context) =>
        ConfirmLeaveDialog(
          askToSave: widget.mode == PlayMode.online,
          onConfirm: (chooseSave) => _exitScreen(chooseSave),
        )
    );
  }

  void _launchGameStatusSideEffect(GameStatus? gameStatus, LobbyGameModel? startingData) {
    final playViewModel = Provider.of<PlayViewModel>(context, listen: false);

    if (gameStatus == GameStatus.starting) {
      final userKey = Provider.of<UserViewModel>(context, listen: false).userData?.id;
      playViewModel.setupGame(
          mode: widget.mode, startingData: startingData, userKey: userKey
      );

    } else if (gameStatus == GameStatus.finishing) {
      playViewModel.doneShowingGameOverDialog();
      showDialog(context: context, builder: (context) =>
          GameOverDialog(
            mode: widget.mode,
            onExit: (chooseSave) => _exitScreen(chooseSave),
          )
      );
    }
  }

  void _listenForConnectionStatus(ConnectionViewModel connectionViewModel) {
    connectionViewModel.listenToConnectionStatus();
    if (widget.mode != PlayMode.offline
        && connectionViewModel.isConnected == false
        && !showDisconnectedDialog
    ) {
      showDisconnectedDialog = true;
      showDialog(context: context, builder: (BuildContext context) {
        return DisconnectedDialog(
          canNavigateBack: widget.mode != PlayMode.multiplayer,
          onClickBack: () {
            showDisconnectedDialog = false;
            _exitScreen(true);
          },
          onReconnected: () => showDisconnectedDialog = false,
        );
      });
    }
  }

  void _drawFromDrawPile(PlayerModel curPlayerData, CardModel drawPileTop) {
    if (curPlayerData.isDrawing()) {
      Provider.of<PlayViewModel>(context, listen: false)
          .drawFromDrawPile(PlayViewModel.userId, drawPileTop, widget.mode);
    }
  }

  void _exitScreen(bool chooseSave) {
    Provider.of<PlayViewModel>(context, listen: false)
        .leaveScreen(widget.mode, chooseSave);
    // Needs to wait for state-setting is finished or else the app will crash
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      if (context.mounted) Navigator.pop(context);
    });
  }
}