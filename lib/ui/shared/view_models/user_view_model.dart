import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/usecases/change_user_name_use_case.dart';
import 'package:card_flutter/domain/usecases/get_anonymous_user_use_case.dart';
import 'package:flutter/foundation.dart';

class UserViewModel extends ChangeNotifier {
  // Dependencies
  final GetAnonymousUserUseCase _getAnonymousUser;
  final ChangeUserNameUseCase _changeUserName;
  // States
  AnonymousUserModel? _userData;
  bool _isLoading = false;

  // Constructor
  UserViewModel(this._getAnonymousUser, this._changeUserName) {
    signIn();
  }

  // State getters
  AnonymousUserModel? get userData {
    return _userData;
  }

  bool get isLoading {
    return _isLoading;
  }

  // State update functions
  Future<void> signIn() async {
    _userData = await _getAnonymousUser.invoke();
    notifyListeners();
  }

  Future<void> modifyUserName(String newName) async {
    _isLoading = true;
    notifyListeners();

    AnonymousUserModel? updatedUserData = await _changeUserName.invoke(newName);
    if (updatedUserData != null) {
      _userData = updatedUserData;
    }
    _isLoading = false;
    notifyListeners();
  }
}