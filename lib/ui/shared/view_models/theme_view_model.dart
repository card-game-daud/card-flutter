import 'package:card_flutter/ui/shared/theme/chosen_theme.dart';
import 'package:flutter/foundation.dart';

import '../../../data/shared_preferences.dart';

class ThemeViewModel with ChangeNotifier {
  // Dependencies
  AppSharedPreferences appSharedPreferences;
  // States
  ChosenTheme _chosenTheme = ChosenTheme.system;

  ThemeViewModel(this.appSharedPreferences) {
    _loadFromSharedPreferences();
  }

  ChosenTheme get currentTheme {
    return _chosenTheme;
  }

  void changeTheme(ChosenTheme newTheme) {
    appSharedPreferences.changeTheme(newTheme);
    _loadFromSharedPreferences();
  }

  Future<void> _loadFromSharedPreferences() async {
    ChosenTheme? savedTheme = await appSharedPreferences.loadTheme();
    if (savedTheme != null) {
      _chosenTheme = savedTheme;
      notifyListeners();
    }
  }
}