import 'package:flutter/material.dart';

import '../../../data/repository/connection_repository.dart';

class ConnectionViewModel extends ChangeNotifier {
  // Dependencies
  final ConnectionRepository _connectionRepository;

  // States
  bool? _isConnected;

  // Constructors
  ConnectionViewModel(this._connectionRepository);

  // State getters
  bool? get isConnected => _isConnected;

  // Methods
  void listenToConnectionStatus() {
    _connectionRepository.listenToConnectionStatus((isConnected) {
          _isConnected = isConnected;
          notifyListeners();
        }
    );
  }
}