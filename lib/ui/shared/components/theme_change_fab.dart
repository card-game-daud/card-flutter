import 'package:card_flutter/ui/shared/components/theme_change_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../theme/chosen_theme.dart';
import '../view_models/theme_view_model.dart';

class ThemeChangeFloatingActionButton extends StatefulWidget {
  const ThemeChangeFloatingActionButton({super.key});

  @override
  State<StatefulWidget> createState() => _ThemeChangeFABState();
}

class _ThemeChangeFABState extends State<ThemeChangeFloatingActionButton> {
  void _openChangeThemeDialog(ChosenTheme curTheme) {
    showDialog(
        context: context,
        builder: (BuildContext context) => ThemeChangeDialog(curTheme: curTheme)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeViewModel>(
      builder: (BuildContext context, vm, Widget? child) {
        return FloatingActionButton(
            onPressed: () { _openChangeThemeDialog(vm.currentTheme); },
            child: Icon(
              switch (vm.currentTheme) {
                ChosenTheme.dark => Icons.dark_mode,
                ChosenTheme.light => Icons.light_mode,
                ChosenTheme.system => Icons.star_half,
              },
              semanticLabel: "The current theme is ${vm.currentTheme.name}. Click to change it.",
            )
        );
      },
    );
  }
}