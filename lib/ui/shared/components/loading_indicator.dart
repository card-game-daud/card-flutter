import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
        width: 48.0,
        height: 48.0,
        child: Center(
          child: CircularProgressIndicator(
            value: null,
          ),
        )
    );
  }
}