import 'package:card_flutter/ui/shared/theme/chosen_theme.dart';
import 'package:card_flutter/ui/shared/view_models/theme_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ThemeChangeDialog extends StatefulWidget {
  final ChosenTheme curTheme;

  const ThemeChangeDialog({super.key, required this.curTheme});

  @override
  State<StatefulWidget> createState() => _ThemeChangeDialogState();
}

class _ThemeChangeDialogState extends State<ThemeChangeDialog> {
  ChosenTheme? _chosenTheme;

  @override
  void initState() {
    super.initState();
    _chosenTheme = widget.curTheme;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Change Theme"),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RadioListTile(
              value: ChosenTheme.dark,
              groupValue: _chosenTheme,
              onChanged: (choice) => setState(() {
                _chosenTheme = choice;
              }),
              title: const Text("Dark"),
            ),
            RadioListTile(
              value: ChosenTheme.light,
              groupValue: _chosenTheme,
              onChanged: (choice) => setState(() {
                _chosenTheme = choice;
              }),
              title: const Text("Light"),
            ),
            RadioListTile(
              value: ChosenTheme.system,
              groupValue: _chosenTheme,
              onChanged: (choice) => setState(() {
                _chosenTheme = choice;
              }),
              title: const Text("System"),
            )
          ],
        ),
      ),
      actions: [
        Consumer<ThemeViewModel>(
            builder: (context, vm, child) {
              return TextButton(
                  onPressed: () {
                    var newTheme = _chosenTheme;
                    if (newTheme != null && newTheme != widget.curTheme) {
                      vm.changeTheme(newTheme);
                    }
                    Navigator.pop(context);
                  },
                  child: const Text("OK")
              );
            }
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          style: TextButtonTheme.of(context).style?.copyWith(
              foregroundColor: MaterialStatePropertyAll(Theme.of(context).colorScheme.error)
          ),
          child: const Text("Cancel"),
        )
      ],
    );
  }
}