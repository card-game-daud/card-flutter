import 'package:card_flutter/ui/shared/view_models/connection_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DisconnectedDialog extends StatelessWidget {
  final bool canNavigateBack;
  final Function() onClickBack;
  final Function() onReconnected;
  
  const DisconnectedDialog({
    super.key,
    required this.canNavigateBack,
    required this.onClickBack,
    required this.onReconnected
  });

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: AlertDialog(
        title: const Text("Lost Connection"),
        content: Consumer<ConnectionViewModel>(
          builder: (BuildContext context, viewModel, Widget? child) {
            viewModel.listenToConnectionStatus();
            if (viewModel.isConnected == true && context.mounted) {
              Navigator.pop(context);
              onReconnected();
            }

            return Text(
              "You have lost connection to the database."
                  " Make sure you are connected to the Internet"
                  "${(canNavigateBack) ? " or click below to return to the previous screen" : ""}.",
              textAlign: TextAlign.justify,
            );
          },
        ),
        actions: [
          if (canNavigateBack)
            ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                  onClickBack();
                },
                child: const Text("Go Back")
            )
        ],
      )
    );
  }
}