import 'package:card_flutter/data/repository/online_play_repository.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:flutter/material.dart';

class HomeViewModel extends ChangeNotifier {
  // Dependencies
  final OnlinePlayRepository _onlinePlayRepository;

  // States
  bool _isLoading = false;

  // Constructors
  HomeViewModel(this._onlinePlayRepository);

  // State getters
  bool get isLoading => _isLoading;
  
  // View-model methods
  Future<void> checkOngoingGame({
    required String userId, required Function(LobbyGameModel) onHasOngoingGame
  }) async {
    _isLoading = true;
    notifyListeners();

    final ongoingGame = await _onlinePlayRepository.getOngoingGameOfUser(userId);
    if (ongoingGame != null && ongoingGame.isMultiplayer) {
      onHasOngoingGame(ongoingGame);
    }

    _isLoading = false;
    notifyListeners();
  }
}