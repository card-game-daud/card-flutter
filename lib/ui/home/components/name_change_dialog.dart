import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NameChangeDialog extends StatelessWidget {
  final String curName;

  const NameChangeDialog({super.key, required this.curName});

  @override
  Widget build(BuildContext context) {
    var controller = TextEditingController(text: curName);
    return Center(
        child: SingleChildScrollView(
            child: AlertDialog(
              title: const Text("Change Your Name"),
              content: TextField(controller: controller),
              actions: [
                TextButton(
                  onPressed: () => _onConfirmChange(context, controller),
                  child: const Text("OK")
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text("Cancel"),
                )
              ],
            )
        )
    );
  }

  void _onConfirmChange(BuildContext context, TextEditingController controller) {
    if (controller.value.text != curName) {
      Provider.of<UserViewModel>(context, listen: false)
          .modifyUserName(controller.value.text);
    }
    Navigator.pop(context);
  }
}