import 'package:flutter/material.dart';

class FailedSignInDisplay extends StatefulWidget {
  final VoidCallback onClickRetry;

  const FailedSignInDisplay({super.key, required this.onClickRetry});

  @override
  State<StatefulWidget> createState() => _FailedSignInState();
}

class _FailedSignInState extends State<FailedSignInDisplay> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.background,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Sambungan gagal", style: Theme.of(context).textTheme.bodyLarge,),
          const SizedBox(height: 8.0),
          ElevatedButton(
              onPressed: widget.onClickRetry,
              child: const Text("Coba lagi")
          )
        ],
      ),
    );
  }
}