import 'package:card_flutter/ui/play/play_route.dart';
import 'package:flutter/material.dart';

class SinglePlayerChoicesDialog extends StatelessWidget {
  const SinglePlayerChoicesDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Single Player", textAlign: TextAlign.center,),
      actions:  [
        ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.push(context, PlayScreenRoute.generateOfflinePageRoute());
            },
            child: const Text("Offline")
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
            Navigator.push(context, PlayScreenRoute.generateOnlinePageRoute());
          },
          child: const Text("Online"),
        )
      ],
      actionsAlignment: MainAxisAlignment.center,
    );
  }
}