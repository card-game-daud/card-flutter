import 'dart:developer';

import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/ui/home/components/failed_sign_in_display.dart';
import 'package:card_flutter/ui/home/components/name_change_dialog.dart';
import 'package:card_flutter/ui/home/components/single_player_choices_dialog.dart';
import 'package:card_flutter/ui/home/home_view_model.dart';
import 'package:card_flutter/ui/lobby/lobby_screen.dart';
import 'package:card_flutter/ui/play/play_route.dart';
import 'package:card_flutter/ui/shared/components/theme_change_fab.dart';
import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import '../shared/components/loading_indicator.dart';
import 'components/signed_in_home_content.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool hasCheckedOngoingGame = false;

  bool get _isLoading =>
      Provider.of<UserViewModel>(context).isLoading
          || Provider.of<HomeViewModel>(context).isLoading;

  void _openChangeNameDialog(String curName) {
    showDialog(
        context: context, builder: (BuildContext context) => NameChangeDialog(curName: curName)
    );
  }

  void _openSinglePlayerChoicesDialog() {
    showDialog(
        context: context, builder: (context) => const SinglePlayerChoicesDialog()
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      fit: StackFit.expand,
      children: [
        Consumer<UserViewModel>(
            builder: (context, vm, child) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                // For smart cast purpose
                final userData = vm.userData;
                if (userData != null && !hasCheckedOngoingGame) {
                  hasCheckedOngoingGame = true;
                  _checkOngoingGame(userData);
                }
              });

              return (vm.userData != null)
                  ? SignedInHomeContent(
                onChooseSinglePlayer: _openSinglePlayerChoicesDialog,
                onChooseMultiPlayer: () => Navigator.push(
                    context, MaterialPageRoute(
                      builder: (BuildContext context) => const LobbyScreen()
                    )
                ),
                onChooseChangeName: () => _openChangeNameDialog(vm.userData!.name),
              )
                  : FailedSignInDisplay(
                onClickRetry: () => vm.signIn(),
              );
            }
        ),
        const Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ThemeChangeFloatingActionButton()
            ),
          ],
        ),
        if (_isLoading) Container(color: Theme.of(context).colorScheme.background.withOpacity(0.5),),
        if (_isLoading) const LoadingIndicator(),
      ],
    );
  }

  void _checkOngoingGame(AnonymousUserModel userData) {
    Provider.of<HomeViewModel>(context, listen: false)
        .checkOngoingGame(userId: userData.id, onHasOngoingGame: (gameData) {
          log("hasOngoingGame: ${gameData.playerIds}");
          final rejoinRoute = PlayScreenRoute.generateMultiPlayerPageRoute(
              gameData: gameData
          );
          Navigator.push(context, rejoinRoute);
        });
  }
}