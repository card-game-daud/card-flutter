import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/model/player_model.dart';
import 'package:card_flutter/ui/lobby/components/lobby_game_list.dart';
import 'package:card_flutter/ui/lobby/components/lobby_top_bar.dart';
import 'package:card_flutter/ui/shared/view_models/connection_view_model.dart';
import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import '../shared/components/disconnected_dialog.dart';
import 'lobby_view_model.dart';

class LobbyScreen extends StatefulWidget {
  const LobbyScreen({super.key});

  @override
  State<StatefulWidget> createState() => _LobbyScreenState();
}

class _LobbyScreenState extends State<LobbyScreen> {
  bool _showDisconnectedDialog = false;
  
  @override
  Widget build(BuildContext context) {
    const addGameText = "Add Game";

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      if (Provider.of<LobbyViewModel>(context, listen: false).games == null) {
        Provider.of<LobbyViewModel>(context, listen: false).enterLobby();
      }
    });

    return Consumer3<LobbyViewModel, UserViewModel, ConnectionViewModel>(
      builder: (context, lobbyVM, userVM, connectionVM, child) {
        SchedulerBinding.instance.addPostFrameCallback(
          (timeStamp) => _listenForConnectionStatus(connectionVM)
        );
        
        return Scaffold(
          appBar: const LobbyTopBar(),
          body: const LobbyGameList(),
          floatingActionButton: (
              !lobbyVM.hasUserJoined(
                  userVM.userData?.id ?? PlayerModel.defaultHumanId
              )
          ) ? FloatingActionButton.extended(
            onPressed: () => _onClickAddGame(userVM.userData),
            label: const Text(addGameText),
            icon: const Icon(Icons.add, semanticLabel: addGameText),
          ) : null,
        );
      },
    );
  }

  void _onClickAddGame(AnonymousUserModel? userData) {
    if (userData != null) {
      Provider.of<LobbyViewModel>(context, listen: false).addGame(userData);
    }
  }

  void _listenForConnectionStatus(ConnectionViewModel connectionVM) {
    connectionVM.listenToConnectionStatus();
    if (connectionVM.isConnected == false && !_showDisconnectedDialog) {
      _showDisconnectedDialog = true;
      showDialog(context: context, builder: (BuildContext context) {
        return DisconnectedDialog(
          canNavigateBack: true,
          onClickBack: () {
            Navigator.pop(context);
          },
          onReconnected: () => _showDisconnectedDialog = false,
        );
      });
    }
  }
}