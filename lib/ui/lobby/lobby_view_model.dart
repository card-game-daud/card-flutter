import 'package:card_flutter/data/repository/lobby_repository.dart';
import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:flutter/material.dart';

class LobbyViewModel extends ChangeNotifier {
  // Dependencies
  final LobbyRepository _lobbyRepository;

  // States
  List<LobbyGameModel>? games;
  bool _isWaiting = false;

  // Constructor
  LobbyViewModel(this._lobbyRepository);

  // State getters
  bool get isWaiting => _isWaiting;

  // State setters
  set isWaiting(bool newVal) {
    _isWaiting = newVal;
    notifyListeners();
  }

  // Methods to connect to domain/data layer
  void enterLobby() {
    isWaiting = true;

    _lobbyRepository.getLobbyData((lobbyGames) {
      games = lobbyGames;
      notifyListeners();
      if (isWaiting) isWaiting = false;
    });
  }

  void addGame(AnonymousUserModel user) {
    _lobbyRepository.addGame(user);
  }

  void joinGame(AnonymousUserModel user, String gameId) {
    _lobbyRepository.joinGame(user, gameId);
  }
  
  void cancelGame(String userId, String gameId) {
    _lobbyRepository.cancelGame(userId, gameId);
  }

  void startGame(String gameId) {
    _lobbyRepository.startGame(gameId);
  }

  // Methods for logic
  bool hasUserJoined(String userId) {
    final games = this.games;
    if (games != null) {
      for (final game in games) {
        if (game.players.map((e) => e.id).contains(userId)) {
          return true;
        }
      }
    }
    return false;
  }
}