import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/ui/lobby/lobby_view_model.dart';
import 'package:card_flutter/ui/shared/theme/button_theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CancelGameDialog extends StatelessWidget {
  final LobbyGameModel game;
  final String userId;

  const CancelGameDialog({super.key, required this.game, required this.userId});

  @override
  Widget build(BuildContext context) {
    final isHost = game.host.id == userId;

    return AlertDialog(
      title: Text((isHost) ? "Cancel Game" : "Cancel Join"),
      content: Text((isHost)
        ? "Are you sure you want to cancel the game?"
        : "Are you sure you no longer want to join the game?"),
      actions: [
        ElevatedButton(
            onPressed: () {
              Provider.of<LobbyViewModel>(context, listen: false).cancelGame(userId, game.id);
              Navigator.pop(context);
            },
            style: generateWarningButtonStyle(context),
            child: const Text("Yes")
        ),
        ElevatedButton(
            onPressed: () => Navigator.pop(context),
            child: const Text("No")
        ),
      ],
      actionsAlignment: MainAxisAlignment.end,
    );
  }
}