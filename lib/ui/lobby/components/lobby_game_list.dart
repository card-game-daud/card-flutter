import 'package:card_flutter/domain/model/lobby_game_model.dart';
import 'package:card_flutter/ui/lobby/lobby_view_model.dart';
import 'package:card_flutter/ui/play/play_route.dart';
import 'package:card_flutter/ui/shared/components/loading_indicator.dart';
import 'package:card_flutter/ui/shared/theme/button_theme.dart';
import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'cancel_game_dialog.dart';

class LobbyGameList extends StatelessWidget {
  const LobbyGameList({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<LobbyViewModel>(builder: (context, viewModel, child) {
      // For smart cast purpose
      final games = viewModel.games;
      final isWaiting = viewModel.isWaiting;

      return ListView(
        padding: const EdgeInsets.all(12.0),
        children: [
          Text(
            "Available Game(s)",
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(height: 12.0,),
          if (games != null && games.isNotEmpty)
            ...games
                .where((game) => game.players.isNotEmpty)
                .map((game) => _LobbyGameCard(
                  game: game,
                  showJoinButton: !viewModel.hasUserJoined(
                      Provider.of<UserViewModel>(context).userData!.id
                  )
                ))
          else if (isWaiting)
            const LoadingIndicator()
          else
            const Padding(
              padding: EdgeInsets.all(12.0),
              child: Text(
                "There is no available game to join right now.\nYou can create one using the button below or wait for other people to create one.",
                textAlign: TextAlign.center,
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            )
        ],
      );
    },);
  }
}

class _LobbyGameCard extends StatelessWidget {
  final LobbyGameModel game;
  final bool showJoinButton;

  const _LobbyGameCard({required this.game, required this.showJoinButton});

  @override
  Widget build(BuildContext context) {
    ButtonStyle primaryButtonStyle = ElevatedButton.styleFrom(
        backgroundColor: Theme.of(context).colorScheme.primaryContainer,
        foregroundColor: Theme.of(context).colorScheme.onPrimaryContainer
    );
    ButtonStyle warningButtonStyle = generateWarningButtonStyle(context);

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      final userId = Provider.of<UserViewModel>(context, listen: false).userData?.id;
      if (!game.isWaiting && game.playerIds.contains(userId)) {
        Navigator.push(context, PlayScreenRoute.generateMultiPlayerPageRoute(
            gameData: game
        ));
      }
    });

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(game.id, style: Theme.of(context).textTheme.titleMedium),
            const SizedBox(height: 8.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: Text.rich(
                TextSpan(
                  text: "Players:",
                  children: [
                    TextSpan(text: "\n•\t\t${game.host.name}"),
                    const TextSpan(text: " (host)", style: TextStyle(fontStyle: FontStyle.italic)),
                    ...game.nonHostPlayers.map((player) =>
                        TextSpan(text: "\n•\t\t${player.name}")
                    )
                  ]
                )
              ),
            ),
            Consumer<UserViewModel>(builder: (context, vm, widget) {
              return Padding(
                padding: const EdgeInsets.only(left: 4.0, right: 4.0, top: 8.0),
                child: Wrap(
                  direction: Axis.horizontal,
                  spacing: 8.0,
                  children: [
                    if (showJoinButton) ElevatedButton(
                        onPressed: () =>
                            Provider.of<LobbyViewModel>(context, listen: false)
                                .joinGame(vm.userData!, game.id),
                        style: primaryButtonStyle,
                        child: const Text("Join")
                    ),
                    if (game.host.id == vm.userData?.id) ElevatedButton(
                        onPressed: () =>
                            Provider.of<LobbyViewModel>(context, listen: false)
                                .startGame(game.id),
                        style: primaryButtonStyle,
                        child: const Text("Start")
                    ),
                    if (game.playerIds.contains(vm.userData?.id)) ElevatedButton(
                        onPressed: () =>
                            showCancelGameDialog(context, vm.userData!.id),
                        style: warningButtonStyle,
                        child: const Text("Cancel")
                    )
                  ],
                ),
              );
            })
          ],
        ),
      ),
    );
  }

  void showCancelGameDialog(BuildContext context, String userId) {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            CancelGameDialog(
                game: game,
                userId: userId,
            )
    );
  }
}