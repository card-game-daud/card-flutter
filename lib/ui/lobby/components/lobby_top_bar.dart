import 'package:card_flutter/domain/model/anonymous_user_model.dart';
import 'package:card_flutter/ui/shared/view_models/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LobbyTopBar extends StatelessWidget implements PreferredSizeWidget {
  const LobbyTopBar({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
      title: const Text("Lobby"),
      actions: [
        OutlinedButton(
            onPressed: () {},
            child: Row(
              children: [
                const Icon(Icons.person),
                const SizedBox(width: 8.0),
                Text(Provider.of<UserViewModel>(context).userData?.name ?? AnonymousUserModel.defaultName)
              ],
            )
        )
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}