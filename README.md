# Card

A "41" card game; Flutter version of [the original Android version](https://gitlab.com/card-game-daud/card-jetpack-compose).

Uses Firebase, so Linux and Windows are not supported. Only Android version is tested.

Only offline game mode is working properly.
